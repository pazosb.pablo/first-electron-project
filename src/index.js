'use strict'

// Instanciar objetos 'app' y 'BrowserWindow':
const { app, BrowserWindow } = require('electron')

// Muestra mensaje en consola antes de salir:
app.on('before-quit', () => {
    console.log('Saliendo...')
})

// Ejecutar órdenes cuando la aplicación está lista:
app.on('ready', () => {
    // Crear una ventana:
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        title: 'Hola mundo!',
        center: true,
        maximizable: false,
        show: false
    })

    win.once('ready-to-show', () => {
        win.show()
    })

    win.on('move', () => {
        const position = win.getPosition()
        console.log(`La posición de la ventana es ${position}`)
    })

    // Detectar el cierre de la ventana para cerrar aplicación:
    win.on('close', () => {
        win = null
        app.quit()
    })

    win.loadURL(`file://${__dirname}/renderer/index.html`)
})

